<?php
    $DB_NAME = "mynotes";
    $DB_USER = "root";
    $DB_PASS = "";
    $DB_SERVER_LOC = "localhost";

    $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
    if($_SERVER['REQUEST_METHOD'] == 'POST'){        
        $sql = "SELECT n.id_note, n.title, p.nm_priority, c.nm_category, 
                    CONCAT('http://192.168.43.224/uas-2d-daily-notes-web/images/',photo) AS url, 
                    n.description
                FROM notes n, category c, priority p
                WHERE n.id_priority = p.id_priority and n.id_category = c.id_category
                ORDER BY n.id_note ASC";
        $result = mysqli_query($conn,$sql);
        if(mysqli_num_rows($result)>0){
            header("Access-Control-Allow-Origin: *");
            header("Content-type: application/json; charset=UTF-8");

            $note_data = array();
            while($note = mysqli_fetch_assoc($result)){
                array_push($note_data, $note);
            }
            echo json_encode($note_data);
        }
    }
?>