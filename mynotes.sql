/*
SQLyog Community
MySQL - 10.1.38-MariaDB : Database - mynotes
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`mynotes` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `mynotes`;

/*Table structure for table `category` */

DROP TABLE IF EXISTS `category`;

CREATE TABLE `category` (
  `id_category` int(5) NOT NULL AUTO_INCREMENT,
  `nm_category` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_category`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `category` */

insert  into `category`(`id_category`,`nm_category`) values 
(1,'Tidak ada kategori'),
(2,'Kantor'),
(3,'Pribadi'),
(4,'Urusan Keluarga'),
(5,'Studi');

/*Table structure for table `notes` */

DROP TABLE IF EXISTS `notes`;

CREATE TABLE `notes` (
  `id_note` int(10) NOT NULL AUTO_INCREMENT,
  `id_category` int(5) DEFAULT NULL,
  `id_priority` int(3) DEFAULT NULL,
  `title` varchar(20) DEFAULT NULL,
  `photo` text,
  `description` text,
  PRIMARY KEY (`id_note`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `notes` */

insert  into `notes`(`id_note`,`id_category`,`id_priority`,`title`,`photo`,`description`) values 
(1,5,3,'UAS','uas.jpg','wajib dikerjakan'),
(2,2,1,'Meja Kerja','mejo.png','buat meja komputer'),
(3,3,1,'Minum Kopi','kopi.jpg','temannya sahabat nabi');

/*Table structure for table `priority` */

DROP TABLE IF EXISTS `priority`;

CREATE TABLE `priority` (
  `id_priority` int(3) NOT NULL AUTO_INCREMENT,
  `nm_priority` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_priority`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `priority` */

insert  into `priority`(`id_priority`,`nm_priority`) values 
(1,'Normal'),
(2,'Penting'),
(3,'Sangat penting');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
